from django.db import models

# Create your models here.


# 工单数据表
class WorkOrder(models.Model):
    id = models.AutoField(primary_key=True)
    ip = models.CharField('IP', max_length=15, default='', null=True)
    host = models.CharField('Host', max_length=255, default='', null=True)
    domain = models.CharField('Domain', max_length=255, default='', null=True)
    header = models.CharField('Header', max_length=255, default='', null=True)
    body = models.CharField('Body', max_length=255, default='', null=True)
    title = models.CharField('Title', max_length=255, default='', null=True)
    app = models.CharField('App', max_length=255, default='', null=True)
    time = models.DateTimeField('Time', auto_now_add=True)
    describe = models.TextField('Describe', default='', null=True)

    class Meta:
        db_table = 'work_order'


# 查询数据表
class AssetData(models.Model):
    id = models.AutoField(primary_key=True)
    host = models.CharField('host', max_length=255, default='')
    ip = models.CharField('ip', max_length=255, default='')
    port = models.CharField('port', max_length=255, default='', null=True)
    domain = models.CharField('domain', max_length=255, default='', null=True)
    type = models.CharField('type', max_length=255, default='')
    protocol = models.CharField('protocol', max_length=255, default='')
    server = models.CharField('server', max_length=255, default='', null=True)
    region = models.CharField('region', max_length=255, default='', null=True)
    province = models.CharField('province', max_length=255, default='', null=True)
    city = models.CharField('city', max_length=255, default='', null=True)
    time = models.DateTimeField('time', auto_now_add=True)

    class Meta:
        db_table = 'asset_data'


# 操作日志表
class TimeData(models.Model):
    week = models.CharField('week', max_length=255, default='')
    # AM.
    day_0 = models.IntegerField('day_0', default=0, null=True)
    day_1 = models.IntegerField('day_1', default=0, null=True)
    day_2 = models.IntegerField('day_2', default=0, null=True)
    day_3 = models.IntegerField('day_3', default=0, null=True)
    day_4 = models.IntegerField('day_4', default=0, null=True)
    day_5 = models.IntegerField('day_5', default=0, null=True)
    day_6 = models.IntegerField('day_6', default=0, null=True)
    day_7 = models.IntegerField('day_7', default=0, null=True)
    day_8 = models.IntegerField('day_8', default=0, null=True)
    day_9 = models.IntegerField('day_9', default=0, null=True)
    day_10 = models.IntegerField('day_10', default=0, null=True)
    day_11 = models.IntegerField('day_11', default=0, null=True)
    # PM.
    day_12 = models.IntegerField('day_12', default=0, null=True)
    day_13 = models.IntegerField('day_13', default=0, null=True)
    day_14 = models.IntegerField('day_14', default=0, null=True)
    day_15 = models.IntegerField('day_15', default=0, null=True)
    day_16 = models.IntegerField('day_16', default=0, null=True)
    day_17 = models.IntegerField('day_17', default=0, null=True)
    day_18 = models.IntegerField('day_18', default=0, null=True)
    day_19 = models.IntegerField('day_19', default=0, null=True)
    day_20 = models.IntegerField('day_20', default=0, null=True)
    day_21 = models.IntegerField('day_21', default=0, null=True)
    day_22 = models.IntegerField('day_22', default=0, null=True)
    day_23 = models.IntegerField('day_23', default=0, null=True)

    class Meta:
        db_table = 'time_data'


# 资产数据总和
class AssetTotal(models.Model):
    ip = models.IntegerField('ip', default='', null=True)
    host = models.IntegerField('host', default='', null=True)
    port = models.IntegerField('port', default='', null=True)
    server = models.IntegerField('server', default='', null=True)
    region = models.IntegerField('region', default='', null=True)
    province = models.IntegerField('province', default='', null=True)
    city = models.IntegerField('city', default='', null=True)
    time = models.DateTimeField('time', auto_now_add=True)

    class Meta:
        db_table = 'asset_total'
