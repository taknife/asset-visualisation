// cs-1 雷达图
(function () {
    var chartDom = document.getElementById('cs-1');
    var myChart = echarts.init(chartDom);
    var option;

    $.get('/cs1').done(function (data) {
        option = {
            tooltip: {
                trigger: 'axis'
            },
            legend: {
                data: ['Server']
            },
            radar: {
                // shape: 'circle',
                indicator: data.indicator
            },
            series: [
                {
                    type: 'radar',
                    tooltip: {
                        trigger: 'item'
                    },
                    areaStyle: {},
                    lineStyle: {
                        type: 'dashed'
                    },
                    label: {
                        show: true,
                        formatter: function (params) {
                            return params.value;
                        }
                    },
                    data: data.data
                }
            ]
        };

        option && myChart.setOption(option);
    });
})();


// cs-2
(function () {
    var chartDom = document.getElementById('cs-2');
    var myChart = echarts.init(chartDom);
    var option;

    $.get('/cs2').done(function (data) {
        option = {
            visualMap: {
                type: 'continuous',
                min: 0,
                max: data.max,
                inRange: {
                    color: ['#2F93C8', '#AEC48F', '#FFDB5C', '#F98862']
                }
            },
            tooltip: {},
            series: {
                type: 'sunburst',
                data: data.data,
                radius: [0, '90%'],
                // itemStyle: {
                //     borderRadius: 7,
                //     borderWidth: 2
                // },
                label: {
                    rotate: 'radial',
                    show: false
                }
            }
        };

        option && myChart.setOption(option);
    });

})();


// cs-3
(function () {
    var chartDom = document.getElementById('cs-3');
    var myChart = echarts.init(chartDom);
    var option;

    myChart.showLoading();
    $.getJSON('/static/data/China.json', function (ChinaJson) {
        // test
        // console.log(ChinaJson);
        // end
        myChart.hideLoading();
        echarts.registerMap('China', ChinaJson, {});
        option = {
            title: {
                text: 'Distribution of Assets in China',
                left: 'right'
            },
            tooltip: {
                trigger: 'item',
                showDelay: 0,
                transitionDuration: 0.2
            },
            visualMap: {
                left: 'right',
                min: 0,
                max: 0,
                inRange: {
                    color: [
                        '#e0f3f8',
                        '#abd9e9',
                        '#74add1',
                        '#4575b4',
                        '#313695'
                        // '#313695',
                        // '#4575b4',
                        // '#74add1',
                        // '#abd9e9',
                        // '#e0f3f8',
                        // '#ffffbf',
                        // '#fee090',
                        // '#fdae61',
                        // '#f46d43',
                        // '#d73027',
                        // '#a50026'
                    ]
                },
                text: ['High', 'Low'],
                calculable: true
            },
            toolbox: {
                show: true,
                //orient: 'vertical',
                left: 'left',
                top: 'top',
                feature: {
                    dataView: {readOnly: false},
                    restore: {},
                    saveAsImage: {}
                }
            },
            series: [
                {
                    name: 'China',
                    type: 'map',
                    roam: true,
                    map: 'China',
                    emphasis: {
                        label: {
                            show: true
                        }
                    },
                    data: []
                }
            ]
        };
        myChart.setOption(option);
        // test 2
        // console.log(option);

        if (option) {
            $.get('/cs3').done(function (data) {
                option = {
                    visualMap: {
                        max: data.max,
                        min: data.min
                    },
                    series: {
                        data: data.data
                    }
                };
                option && myChart.setOption(option);
            });
        }
        ;
    });
    option && myChart.setOption(option);

    // $.get('/cs3').done(function(data) {
    //     option = {
    //         visualMap: {
    //             max: data.max,
    //             min: data.min
    //         },
    //         series: {
    //             data: data.data
    //         }
    //     };
    //     option && myChart.setOption(option);
    // });

})();


// cs-4
(function () {
    var chartDom = document.getElementById('cs-4');
    var myChart = echarts.init(chartDom);
    var option;

    $.get('/cs4').done(function(data) {
        option = {
            tooltip: {
                trigger: 'axis',
                axisPointer: {
                    type: 'cross',
                    label: {
                        backgroundColor: '#6a7985'
                    }
                }
            },
            grid: {
                left: '3%',
                right: '4%',
                bottom: '3%',
                top: '10%',
                containLabel: true
            },
            xAxis: [
                {
                    type: 'category',
                    boundaryGap: false,
                    data: ['IP', 'Host', 'Port', 'Server', 'Region', 'Province', 'City']
                }
            ],
            yAxis: [
                {
                    type: 'value'
                }
            ],
            series: data.data
        };

        option && myChart.setOption(option);
    });

})();


// cs-5
(function () {
    var chartDom = document.getElementById('cs-5');
    var myChart = echarts.init(chartDom);
    var option;

    // prettier-ignore
    const hours = [
        '12a', '1a', '2a', '3a', '4a', '5a', '6a',
        '7a', '8a', '9a', '10a', '11a',
        '12p', '1p', '2p', '3p', '4p', '5p',
        '6p', '7p', '8p', '9p', '10p', '11p'
    ];
    // prettier-ignore
    const days = [
        '1', '2', '3', '4', '5', '6', '7'
    ];
    // prettier-ignore
    $.get('/cs5').done(function(data) {
        option = {
            legend: {
                data: ['Punch Card'],
                left: 'right'
            },
            polar: {},
            tooltip: {
                formatter: function (params) {
                    return (
                        params.value[2] +
                        ' commits in ' +
                        hours[params.value[1]] +
                        ' of ' +
                        days[params.value[0]]
                    );
                }
            },
            angleAxis: {
                type: 'category',
                data: hours,
                boundaryGap: false,
                splitLine: {
                    show: true
                },
                axisLine: {
                    show: false
                }
            },
            radiusAxis: {
                type: 'category',
                data: days,
                axisLine: {
                    show: false
                },
                axisLabel: {
                    rotate: 45
                }
            },
            series: [
                {
                    name: 'Punch Card',
                    type: 'scatter',
                    coordinateSystem: 'polar',
                    symbolSize: function (val) {
                        return val[2] * 2;
                    },
                    data: data.data,
                    animationDelay: function (idx) {
                        return idx * 5;
                    }
                }
            ]
        };

        option && myChart.setOption(option);
    });
})();


// cs-6
(function () {
    var chartDom = document.getElementById('cs-6');
    var myChart = echarts.init(chartDom);
    var option;

    option = {
        polar: {
            radius: [30, '80%']
        },
        radiusAxis: {
            max: 30
        },
        angleAxis: {
            type: 'category',
            data: [],
            startAngle: 75
        },
        tooltip: {},
        series: {
            type: 'bar',
            data: [],
            coordinateSystem: 'polar',
            label: {
                show: true,
                position: 'middle',
                formatter: '{b}: {c}'
            }
        },
        backgroundColor: '#fff',
        animation: false
    };
    option && myChart.setOption(option);

    $.get('/cs6').done(function(data) {
        option = {
            radiusAxis: {
                max: data.maxdata
            },
            angleAxis: {
                data: data.radiusaxis_data
            },
            series: {
                data: data.series_data
            },
        };
        option && myChart.setOption(option);
    });
})();