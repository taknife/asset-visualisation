# -*- coding: utf-8 -*-
# @Time : 2022/2/13 0:26
# @Author : Taknife (Huang Yuxuan)
# @Project : fofa-spider
# @File : request_center.py
# @Software : PyCharm

import requests
import json
import logging
import base64
import time

# import traceback

logging.captureWarnings(True)
# 保存会话状态
session = requests.session()
headers = {
    'upgrade-insecure-requests': '1',
    'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/98.0.4758.82 Safari/537.36'
}
email = "wh.huangyuxuan@foxmail.com"
key = "3cb5d94e277f6db7006494ebd0ec8a6f"
# 需要查询的字段
fields_list = ['host', 'ip', 'port', 'domain', 'type', 'protocol', 'server', 'country_name', 'province', 'city']


# 查找字符串的合成
def select_fofa(sqldic):
    sqllist = []
    for sqlkey in sqldic:
        if sqldic[sqlkey]:
            sqlstr = sqlkey + "=\"" + sqldic[sqlkey] + "\""
            sqllist.append(sqlstr)
    result = ' && '.join(sqllist)
    return result


# 生成请求url
def url_fofa(select_str, page=1, size=500, full="true"):
    base_url = f"https://fofa.info/api/v1/search/all?email=" + email + "&key=" + key
    select_byte = select_str.encode()
    qbase64 = base64.b64encode(select_byte)
    qbase64 = qbase64.decode()
    fields = ','.join(fields_list)
    url = base_url + "&qbase64=" + qbase64 + "&page=" + str(page) + "&size=" + str(size) + "&fields=" + fields + "&full=" + full
    return url


# 请求fofa api返回结果
def request_fofa(url):
    end_value = ''
    try:
        response = session.get(url=url, verify=False, headers=headers)
        result = json.loads(response.text)
        total = result['size']
        error = result
        if result['error'] and 'None' not in result['error']:
            # info = u'fofa 错误:' + result['error'] + u' 休眠30s'
            # logging.error(info)
            time.sleep(30)
        else:
            end_value = result
    except Exception as e:
        # logging.error(u'fofa 错误:' + str(e.message) + u' 休眠30s')
        # traceback.print_exc()
        time.sleep(30)
    return end_value


# 格式化字符串
def dic_format(value):
    result = []
    for i in value['results']:
        result.append(tuple(i))
    return result


def main(sqldic):
    select_str = select_fofa(sqldic)
    url = url_fofa(select_str)
    value = request_fofa(url)
    main_result = dic_format(value)
    return main_result
