# -*- coding: utf-8 -*-
# @Time : 2022/4/19 2:14
# @Author : Taknife (Huang Yuxuan)
# @Project : AssetsWeb
# @File : daytime.py
# @Software : PyCharm

from ..models import TimeData
from django.db import connection
import datetime


# 计算时间
def timedata():
    now = datetime.datetime.now()
    year = now.year
    month = now.month
    day = now.day
    hour = now.hour
    minute = now.minute
    week = datetime.datetime(year, month, day).strftime("%A")
    week_list = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"]
    if not TimeData.objects.values('week'):
        for w in week_list:
            TimeData.objects.create(week=w)
    if week == 'Monday' and hour == 0 and minute == 0:
        with connection.cursor() as cur:
            cur.execute('TRUNCATE TABLE `time_data`;')
    return week, hour


# 计算查询次数
def clock_in():
    week, hour = timedata()
    if hour == 0:
        num = TimeData.objects.filter(week=week).values('day_0')[0]['day_0']
        TimeData.objects.filter(week=week).update(day_0=num+1)
    elif hour == 1:
        num = TimeData.objects.filter(week=week).values('day_1')[0]['day_1']
        TimeData.objects.filter(week=week).update(day_1=num+1)
    elif hour == 2:
        num = TimeData.objects.filter(week=week).values('day_2')[0]['day_2']
        TimeData.objects.filter(week=week).update(day_2=num+1)
    elif hour == 3:
        num = TimeData.objects.filter(week=week).values('day_3')[0]['day_3']
        TimeData.objects.filter(week=week).update(day_3=num+1)
    elif hour == 4:
        num = TimeData.objects.filter(week=week).values('day_4')[0]['day_4']
        TimeData.objects.filter(week=week).update(day_4=num+1)
    elif hour == 5:
        num = TimeData.objects.filter(week=week).values('day_5')[0]['day_5']
        TimeData.objects.filter(week=week).update(day_5=num+1)
    elif hour == 6:
        num = TimeData.objects.filter(week=week).values('day_6')[0]['day_6']
        TimeData.objects.filter(week=week).update(day_6=num+1)
    elif hour == 7:
        num = TimeData.objects.filter(week=week).values('day_7')[0]['day_7']
        TimeData.objects.filter(week=week).update(day_7=num+1)
    elif hour == 8:
        num = TimeData.objects.filter(week=week).values('day_8')[0]['day_8']
        TimeData.objects.filter(week=week).update(day_8=num+1)
    elif hour == 9:
        num = TimeData.objects.filter(week=week).values('day_9')[0]['day_9']
        TimeData.objects.filter(week=week).update(day_9=num+1)
    elif hour == 10:
        num = TimeData.objects.filter(week=week).values('day_10')[0]['day_10']
        TimeData.objects.filter(week=week).update(day_10=num+1)
    elif hour == 11:
        num = TimeData.objects.filter(week=week).values('day_11')[0]['day_11']
        TimeData.objects.filter(week=week).update(day_11=num+1)
    elif hour == 12:
        num = TimeData.objects.filter(week=week).values('day_12')[0]['day_12']
        TimeData.objects.filter(week=week).update(day_12=num+1)
    elif hour == 13:
        num = TimeData.objects.filter(week=week).values('day_13')[0]['day_13']
        TimeData.objects.filter(week=week).update(day_13=num+1)
    elif hour == 14:
        num = TimeData.objects.filter(week=week).values('day_14')[0]['day_14']
        TimeData.objects.filter(week=week).update(day_14=num+1)
    elif hour == 15:
        num = TimeData.objects.filter(week=week).values('day_15')[0]['day_15']
        TimeData.objects.filter(week=week).update(day_15=num+1)
    elif hour == 16:
        num = TimeData.objects.filter(week=week).values('day_16')[0]['day_16']
        TimeData.objects.filter(week=week).update(day_16=num+1)
    elif hour == 17:
        num = TimeData.objects.filter(week=week).values('day_17')[0]['day_17']
        TimeData.objects.filter(week=week).update(day_17=num+1)
    elif hour == 18:
        num = TimeData.objects.filter(week=week).values('day_18')[0]['day_18']
        TimeData.objects.filter(week=week).update(day_18=num+1)
    elif hour == 19:
        num = TimeData.objects.filter(week=week).values('day_19')[0]['day_19']
        TimeData.objects.filter(week=week).update(day_19=num+1)
    elif hour == 20:
        num = TimeData.objects.filter(week=week).values('day_20')[0]['day_20']
        TimeData.objects.filter(week=week).update(day_20=num+1)
    elif hour == 21:
        num = TimeData.objects.filter(week=week).values('day_21')[0]['day_21']
        TimeData.objects.filter(week=week).update(day_21=num+1)
    elif hour == 22:
        num = TimeData.objects.filter(week=week).values('day_22')[0]['day_22']
        TimeData.objects.filter(week=week).update(day_22=num+1)
    elif hour == 23:
        num = TimeData.objects.filter(week=week).values('day_23')[0]['day_23']
        TimeData.objects.filter(week=week).update(day_23=num+1)
