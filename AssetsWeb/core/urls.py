# -*- coding: utf-8 -*-
# @Time : 2022/3/22 17:12
# @Author : Taknife (Huang Yuxuan)
# @Project : AssetsWeb
# @File : urls.py
# @Software : PyCharm
from django.urls import path
from . import views

urlpatterns = [
    path('', views.work_order),
    path('work-order.html', views.work_order),
    path('create-work.html', views.create_work),
    path('visualization.html', views.visualization),
    path('analysis.html', views.analysis),
    path('getdata', views.getdata),  # 获取数据
    path('emptydata', views.emptydata),  # 清空数据
    path('deldata', views.deldata),  # 删除数据
    path('sqldata', views.sqldata),  # 查询数据
    path('assetdata', views.assetdata),  # 资产数据
    path('cs1', views.cs1),  # cs-1数据
    path('cs2', views.cs2),  # cs-2数据
    path('cs3', views.cs3),  # cs-3数据
    path('cs4', views.cs4),  # cs-4数据
    path('cs5', views.cs5),  # cs-5数据
    path('cs6', views.cs6),  # cs-6数据
    path('test', views.test),  # 测试数据
]
