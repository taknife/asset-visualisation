# Generated by Django 2.2.26 on 2022-04-18 07:36

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0002_auto_20220418_1533'),
    ]

    operations = [
        migrations.AlterField(
            model_name='timedata',
            name='day_0',
            field=models.IntegerField(default=0, null=True, verbose_name='day_0'),
        ),
        migrations.AlterField(
            model_name='timedata',
            name='day_1',
            field=models.IntegerField(default=0, null=True, verbose_name='day_1'),
        ),
        migrations.AlterField(
            model_name='timedata',
            name='day_10',
            field=models.IntegerField(default=0, null=True, verbose_name='day_10'),
        ),
        migrations.AlterField(
            model_name='timedata',
            name='day_11',
            field=models.IntegerField(default=0, null=True, verbose_name='day_11'),
        ),
        migrations.AlterField(
            model_name='timedata',
            name='day_12',
            field=models.IntegerField(default=0, null=True, verbose_name='day_12'),
        ),
        migrations.AlterField(
            model_name='timedata',
            name='day_13',
            field=models.IntegerField(default=0, null=True, verbose_name='day_13'),
        ),
        migrations.AlterField(
            model_name='timedata',
            name='day_14',
            field=models.IntegerField(default=0, null=True, verbose_name='day_14'),
        ),
        migrations.AlterField(
            model_name='timedata',
            name='day_15',
            field=models.IntegerField(default=0, null=True, verbose_name='day_15'),
        ),
        migrations.AlterField(
            model_name='timedata',
            name='day_16',
            field=models.IntegerField(default=0, null=True, verbose_name='day_16'),
        ),
        migrations.AlterField(
            model_name='timedata',
            name='day_17',
            field=models.IntegerField(default=0, null=True, verbose_name='day_17'),
        ),
        migrations.AlterField(
            model_name='timedata',
            name='day_18',
            field=models.IntegerField(default=0, null=True, verbose_name='day_18'),
        ),
        migrations.AlterField(
            model_name='timedata',
            name='day_19',
            field=models.IntegerField(default=0, null=True, verbose_name='day_19'),
        ),
        migrations.AlterField(
            model_name='timedata',
            name='day_2',
            field=models.IntegerField(default=0, null=True, verbose_name='day_2'),
        ),
        migrations.AlterField(
            model_name='timedata',
            name='day_20',
            field=models.IntegerField(default=0, null=True, verbose_name='day_20'),
        ),
        migrations.AlterField(
            model_name='timedata',
            name='day_21',
            field=models.IntegerField(default=0, null=True, verbose_name='day_21'),
        ),
        migrations.AlterField(
            model_name='timedata',
            name='day_22',
            field=models.IntegerField(default=0, null=True, verbose_name='day_22'),
        ),
        migrations.AlterField(
            model_name='timedata',
            name='day_23',
            field=models.IntegerField(default=0, null=True, verbose_name='day_23'),
        ),
        migrations.AlterField(
            model_name='timedata',
            name='day_3',
            field=models.IntegerField(default=0, null=True, verbose_name='day_3'),
        ),
        migrations.AlterField(
            model_name='timedata',
            name='day_4',
            field=models.IntegerField(default=0, null=True, verbose_name='day_4'),
        ),
        migrations.AlterField(
            model_name='timedata',
            name='day_5',
            field=models.IntegerField(default=0, null=True, verbose_name='day_5'),
        ),
        migrations.AlterField(
            model_name='timedata',
            name='day_6',
            field=models.IntegerField(default=0, null=True, verbose_name='day_6'),
        ),
        migrations.AlterField(
            model_name='timedata',
            name='day_7',
            field=models.IntegerField(default=0, null=True, verbose_name='day_7'),
        ),
        migrations.AlterField(
            model_name='timedata',
            name='day_8',
            field=models.IntegerField(default=0, null=True, verbose_name='day_8'),
        ),
        migrations.AlterField(
            model_name='timedata',
            name='day_9',
            field=models.IntegerField(default=0, null=True, verbose_name='day_9'),
        ),
    ]
