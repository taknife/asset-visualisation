# Generated by Django 2.2.26 on 2022-04-20 16:43

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0005_assettotal'),
    ]

    operations = [
        migrations.AddField(
            model_name='assettotal',
            name='time',
            field=models.CharField(default='', max_length=255, verbose_name='time'),
        ),
    ]
