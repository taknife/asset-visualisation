from django.shortcuts import render
from django.http import HttpResponseRedirect, JsonResponse
from .models import WorkOrder, AssetData, TimeData, AssetTotal
from django.db import connection
from core.assetspider import request_center
from core.coretools import daytime


# Create your views here.
# ----- 工单页面 -----
def work_order(request):
    return render(request, 'work-order.html')


def create_work(request):
    if request.method == 'GET':
        return render(request, 'create-work.html')
    elif request.method == 'POST' and request.POST:
        work = {
            'ip': request.POST.get('ip'),
            'host': request.POST.get('host'),
            'domain': request.POST.get('domain'),
            'header': request.POST.get('header'),
            'body': request.POST.get('body'),
            'title': request.POST.get('title'),
            'app': request.POST.get('app'),
            'describe': request.POST.get('describe')
        }
        for value in work.values():
            if value:
                WorkOrder.objects.create(
                    ip=work['ip'],
                    host=work['host'],
                    domain=work['domain'],
                    header=work['header'],
                    body=work['body'],
                    title=work['title'],
                    app=work['app'],
                    describe=work['describe']
                )
                break
            else:
                pass

        # return render(request, 'work-order.html', locals())
        return HttpResponseRedirect('work-order.html')


# 在工单页面获取数据表
def getdata(request):
    data = []
    result = WorkOrder.objects.all()
    for i in range(len(result)):
        event = {
            'id': result[i].id,
            'ip': result[i].ip,
            'host': result[i].host,
            'domain': result[i].domain,
            'header': result[i].header,
            'body': result[i].body,
            'title': result[i].title,
            'app': result[i].app,
            'time': result[i].time.strftime("%Y-%m-%d %H:%M:%S"),
            'describe': result[i].describe
        }
        data.append(event)
    count = len(data)
    if count != 0:
        return JsonResponse({'code': 0, 'msg': '获取成功', 'count': count, 'data': data})
    else:
        return JsonResponse({'code': 0, 'msg': '获取失败'})


# 清空数据表
def emptydata(request):
    with connection.cursor() as cur:
        cur.execute('TRUNCATE TABLE `work_order`;')
    return HttpResponseRedirect('work-order.html')


# 删除数据
def deldata(request):
    if request.method == 'POST':
        work_id = request.POST.get('work_id')
        obj = WorkOrder.objects.filter(id=work_id)
        obj.delete()
        with connection.cursor() as cur:
            cur.execute('SET @rownum = 0;')
            cur.execute('UPDATE work_order SET id = @rownum := @rownum +1;')
            cur.execute('SELECT max(id)+1 from work_order;')
            cur.execute('alter table work_order auto_increment=1;')
        return HttpResponseRedirect('work-order.html')


# 查询数据
def sqldata(request):
    if request.method == 'POST':
        # 查询数据
        work_id = request.POST.get('work_id')
        sqldic = {
            'ip': WorkOrder.objects.get(id=work_id).ip,
            'host': WorkOrder.objects.get(id=work_id).host,
            'domain': WorkOrder.objects.get(id=work_id).domain,
            'header': WorkOrder.objects.get(id=work_id).header,
            'body': WorkOrder.objects.get(id=work_id).body,
            'title': WorkOrder.objects.get(id=work_id).title,
            'app': WorkOrder.objects.get(id=work_id).app
        }
        result = request_center.main(sqldic)
        with connection.cursor() as cur:
            cur.execute('TRUNCATE TABLE `asset_data`;')
        for i in result:
            AssetData.objects.create(
                host=i[0],
                ip=i[1],
                port=i[2],
                domain=i[3],
                type=i[4],
                protocol=i[5],
                server=i[6],
                region=i[7],
                province=i[8],
                city=i[9]
            )
        daytime.clock_in()
        return HttpResponseRedirect('analysis.html')


# ----- 资产可视化分析页面 -----
def visualization(request):
    return render(request, 'visualization.html')


# ----- cs-1 图表数据 -----
def cs1(request):
    server_all = []
    server_data = []
    order = []
    cs1datadic = {
        'indicator': [],
        'data': []
    }
    result = AssetData.objects.values('server')
    for i in result:
        server_all.append(i['server'])
        if i['server'] not in server_data:
            server_data.append(i['server'])
    server_dic = dict.fromkeys(server_data)
    for key in server_dic:
        n = 0
        for j in server_all:
            if key == j:
                n += 1
        server_dic[key] = n
    d_order = sorted(server_dic.items(), key=lambda x: x[1], reverse=True)

    for j in d_order:
        list(j)
        k = ['unknow' if x == '' else x for x in j]
        j = tuple(k)
        order.append(j)

    if len(order) >= 8:
        data = {
            'value': [],
            'name': 'Server'
        }
        for i in range(8):
            data_sub = {
                'name': order[i][0],
                'max': int(order[i][1] * 3 / 2 + 1)
            }
            cs1datadic['indicator'].append(data_sub)
            data['value'].append(order[i][1])
        cs1datadic['data'].append(data)
    else:
        data = {
            'value': [],
            'name': 'Server'
        }
        for i in range(len(order)):
            data_sub = {
                'name': order[i][0],
                'max': int(order[i][1] * 3 / 2 + 1)
            }
            cs1datadic['indicator'].append(data_sub)
            data['value'].append(order[i][1])
        cs1datadic['data'].append(data)

    return JsonResponse(cs1datadic)


# ----- cs-2 图表数据 -----
def cs2(request):
    data = []
    region_data = []
    region_all = []
    province_data = []
    province_all = []
    province_children = []
    city_data = []
    city_all = []
    city_children = []

    result = AssetData.objects.values('region', 'province', 'city')

    for i in result:
        if i['region']:
            region_all.append(i['region'])
        if i['region'] not in region_data and i['region']:
            region_data.append(i['region'])
        if i['province']:
            province_all.append(i['province'])
        if i['province'] not in province_data and i['province']:
            province_data.append(i['province'])
        if i['city']:
            city_all.append(i['city'])
        if i['city'] not in city_data and i['city']:
            city_data.append(i['city'])

    for city_name in city_data:
        data_sub = {
            'name': city_name,
            'value': city_all.count(city_name)
        }
        city_children.append(data_sub)

    for province_name in province_data:
        data_sub = {
            'name': province_name,
            'value': province_all.count(province_name),
            'children': []
        }
        province_children.append(data_sub)

    for region_name in region_data:
        data_sub = {
            'name': region_name,
            'value': region_all.count(region_name),
            'children': []
        }
        data.append(data_sub)

    for j in data:
        for k in result:
            if j['name'] == k['region']:
                for l in province_children:
                    if k['province'] == l['name'] and l not in j['children']:
                        j['children'].append(l)

    for m in city_children:
        for k in result:
            if m['name'] == k['city']:
                for l in data:
                    for x in l['children']:
                        if k['province'] == x['name'] and m not in x['children']:
                            x['children'].append(m)

    rmax = 10;
    for z in data:
        if z['value'] > rmax:
            rmax = z['value']

    cs2datadic = {
        'max': rmax,
        'data': data
    }

    return JsonResponse(cs2datadic)


# ----- cs-3 图表数据 -----
def cs3(request):
    data = []
    region1_all = []
    region1_data = []
    region2_all = []
    region2_data = []
    result = AssetData.objects.filter(region__in=['China', 'Taiwan', 'Hong Kong', 'Macao']).values('region', 'province')
    for i in result:
        if i['region'] == 'China' and i['province'] != '':
            region1_all.append(i['province'])
            if i['province'] not in region1_data:
                region1_data.append(i['province'])
        elif i['region'] != '' and i['region'] != 'China':
            region2_all.append(i['region'])
            if i['region'] not in region2_data:
                region2_data.append(i['region'])

    for j in region1_data:
        data_sub = {
            'name': j,
            'value': region1_all.count(j)
        }
        data.append(data_sub)

    for k in region2_data:
        data_sub = {
            'name': k,
            'value': region2_all.count(k)
        }
        data.append(data_sub)

    vmax = data[0]['value']
    vmin = data[0]['value']
    for l in range(0, len(data)-1):
        if vmax < data[l+1]['value']:
            vmax = data[l+1]['value']
        if vmin > data[l+1]['value']:
            vmin = data[l+1]['value']

    if vmax == vmin:
        vmin = 0

    cs3datadic = {
        'data': data,
        'max': vmax,
        'min': vmin
    }
    return JsonResponse(cs3datadic)


# ----- cs-4 图表数据 -----
def cs4(request):
    result = AssetData.objects.values('ip', 'host', 'port', 'server', 'region', 'province', 'city')
    key = ['ip', 'host', 'port', 'server', 'region', 'province', 'city']
    asset_all = []
    asset_data = []
    data = []
    for k in key:
        for i in result:
            asset_all.append(i[k])
        for j in asset_all:
            if j not in asset_data and j != '':
                asset_data.append(j)
        data.append(len(asset_data))
        asset_all.clear()
        asset_data.clear()
    # print(data)
    # print(len(AssetTotal.objects.values('id')))
    if len(AssetTotal.objects.values('id')) < 5:
        AssetTotal.objects.create(
            ip=data[0],
            host=data[1],
            port=data[2],
            server=data[3],
            region=data[4],
            province=data[5],
            city=data[6]
        )
    else:
        del_data = AssetTotal.objects.get(id=1)
        del_data.delete()
        with connection.cursor() as cur:
            cur.execute('SET @rownum = 0;')
            cur.execute('UPDATE asset_total SET id = @rownum := @rownum +1;')
            cur.execute('SELECT max(id)+1 from asset_total;')
            cur.execute('alter table asset_total auto_increment=1;')
        AssetTotal.objects.create(
            ip=data[0],
            host=data[1],
            port=data[2],
            server=data[3],
            region=data[4],
            province=data[5],
            city=data[6]
        )

    final_data = AssetTotal.objects.values('ip', 'host', 'port', 'server', 'region', 'province', 'city', 'time')

    finish_data = []
    for i in final_data:
        finish_data_sub = {
            'name': i['time'].strftime("%Y-%m-%d %H:%M:%S"),
            'type': 'line',
            'stack': 'Total',
            'areaStyle': {},
            'emphasis': {
                'focus': 'series'
            },
            'data': []
        }
        value_list = list(i.values())
        value_list.pop()
        finish_data_sub['data'] = value_list
        finish_data.append(finish_data_sub)

    cs4datadic = {
        'data': finish_data
    }
    return JsonResponse(cs4datadic)
    # return HttpResponseRedirect('analysis.html')


# ----- cs-5 图表数据 -----
def cs5(request):
    data = []
    week = TimeData.objects.values('week')
    a = 0
    for w in week:
        for i in range(0, 24):
            day_time = 'day_' + str(i)
            n = TimeData.objects.filter(week=w['week']).values(day_time)[0][day_time]
            if n != 0:
                data_sub = [a, i, n]
                data.append(data_sub)
        a += 1
    cs5datadic = {
        'data': data
    }
    return JsonResponse(cs5datadic)


# ----- cs-6 图表数据 -----
def cs6(request):
    alldata = []
    data = []
    radiusaxis = []
    series = []
    result = AssetData.objects.all()
    for i in result:
        alldata.append(i.port)
        if i.port not in data:
            data.append(i.port)
    datadic = dict.fromkeys(data)
    for key in datadic:
        n = 0
        for j in alldata:
            if key == j:
                n += 1
        datadic[key] = n
    d_order = sorted(datadic.items(), key=lambda x: x[1], reverse=True)
    if len(d_order) >= 5:
        for i in range(5):
            radiusaxis.append(d_order[i][0])
            series.append(d_order[i][1])
    else:
        for i in range(len(d_order)):
            radiusaxis.append(d_order[i][0])
            series.append(d_order[i][1])
    cs6datadic = {
        "maxdata": series[0] + 5,
        "radiusaxis_data": radiusaxis,
        "series_data": series
    }

    return JsonResponse(cs6datadic)


# ----- 资产详情页面 -----
def analysis(request):
    return render(request, 'analysis.html')


def assetdata(request):
    data = []
    result = AssetData.objects.all()
    for i in range(len(result)):
        event = {
            'id': result[i].id,
            'host': result[i].host,
            'ip': result[i].ip,
            'port': result[i].port,
            'domain': result[i].domain,
            'type': result[i].type,
            'protocol': result[i].protocol,
            'server': result[i].server,
            'region': result[i].region,
            'province': result[i].province,
            'city': result[i].city,
            'time': result[i].time.strftime("%Y-%m-%d %H:%M:%S")
        }
        data.append(event)
    count = len(data)
    if count != 0:
        return JsonResponse({'code': 0, 'msg': '获取成功', 'count': count, 'data': data})
    else:
        return JsonResponse({'code': 0, 'msg': '获取失败'})


# ----- test -----
def test(request):
    # print(timedata())
    # clock_in()
    return HttpResponseRedirect('analysis.html')
